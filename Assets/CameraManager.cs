﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class CameraManager : MonoBehaviour {



        public float horizontalResolution = 1920;

        void OnGUI()
        {
        //
        float currentAspect = (float)Screen.width / (float)Screen.height;
        Camera.main.orthographicSize = horizontalResolution / currentAspect / 200;
        //Another way. Not so good as upper method
        //float s_baseOrthographicSize = Screen.height / 64.0f / 2.0f;
        //Camera.main.orthographicSize = s_baseOrthographicSize;
        //Another one
        //Camera.main.orthographicSize = Screen.width / (((Screen.width / Screen.height) * 2) * 32);

    }
}
