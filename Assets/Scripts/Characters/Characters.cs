﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MainCharactersEnum
{
    Swordmaster,
    Bowmaster,
    Staffmaster
}

[CreateAssetMenu(fileName = "New dialogue characters", menuName = "Characters dialogue/New character dialogue")]
public class Characters : ScriptableObject {

    public MainCharactersEnum characterType;
    [TextArea]
    public List<string> CharactersDialogues = new List<string>();

}
