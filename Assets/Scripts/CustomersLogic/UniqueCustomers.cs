﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New unique customer", menuName = "Unique customers/New customer")]
public class UniqueCustomers : ScriptableObject {

    public string uniqueCustomerName;
    [TextArea]
    public List<string> dialoguesList = new List<string>();

    public Item.ItemTypes requiredWeaponType;
    public Item.Elements requiredElementType;

}
