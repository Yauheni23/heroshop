﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New news", menuName = "News/Important News")]
public class NewsScript : ScriptableObject {

    public string NewsHeader;
    [TextArea]
    public string NewsText;
    public enum ChangableMaterial
    {
        Metall,
        Wood,
        Crystall,
        Bowstring,
        ExtraMetall
    }
    public ChangableMaterial changableMaterial;
    public int changePricePercent;

}
