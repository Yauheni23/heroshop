﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Assets.Scripts.LogicScripts;

public class CreateWeaponPanelScript : MonoBehaviour {


    public Item.ItemTypes currentType;
    public TMP_InputField weaponName;
    public Slider attackSlider;
    public Slider attackSpeedSlider;
    public Slider criticalAttackSlider;

    //Element toggles
    public Toggle fireToggle;
    public Toggle iceToggle;
    public Toggle electricityToggle;

    public Button createWeaponButton;
    public Button closeWindowButton;

    //MaterialsUI
    public TMP_Text attackPercent;
    public TMP_Text attackSpeedPercent;
    public TMP_Text criticalAttackPercent;

    //Profit percent toggles
    public Toggle fivePercentProfitToggle;
    public Toggle tenPercentProfitToggle;
    public Toggle fifteenPercentProfitToggle;
    public Toggle thirtyPercentProfitToggle;

    //Price calculations
    private int currentPrice;
    private int profitSumm;

    //Tax
    private int taxPercent;
    private int taxSumm;

    //Slider results
    private int attackResult;
    private int attackSpeedResult;
    private int critResult;
    private Item.Elements choisenElement;

    //INfo about materials cost
    private int attackMaterialPrice = 5;
    private int attackSpeedMaterialPrice = 10;
    private int attackCriticalMaterialPrice = 15;

    //price panel text
    public TMP_Text profitSummText;
    public TMP_Text taxSummText;
    public TMP_Text totalSummText;

    public TMP_Text taxPercentText;

    public void Start()
    {
        ValueSliderChangeCheck(attackSlider);
        attackSlider.onValueChanged.AddListener(delegate { ValueSliderChangeCheck(attackSlider); });
        attackSpeedSlider.onValueChanged.AddListener(delegate { ValueSliderChangeCheck(attackSpeedSlider); });
        criticalAttackSlider.onValueChanged.AddListener(delegate { ValueSliderChangeCheck(criticalAttackSlider); });
        createWeaponButton.onClick.AddListener(HandleCreateItem);
        closeWindowButton.onClick.AddListener(HandleCloseItemCreationWindow);

        attackResult = (int)attackSlider.value;
        attackSpeedResult = (int)attackSpeedSlider.value;
        critResult = (int)criticalAttackSlider.value;

        //Toggles listeners
        fireToggle.onValueChanged.AddListener(delegate { CalculateItemPrice(); });
        iceToggle.onValueChanged.AddListener(delegate { CalculateItemPrice(); });
        electricityToggle.onValueChanged.AddListener(delegate { CalculateItemPrice(); });

        fivePercentProfitToggle.onValueChanged.AddListener(delegate { CalculateItemPrice(); });
        tenPercentProfitToggle.onValueChanged.AddListener(delegate { CalculateItemPrice(); });
        fifteenPercentProfitToggle.onValueChanged.AddListener(delegate { CalculateItemPrice(); });
        thirtyPercentProfitToggle.onValueChanged.AddListener(delegate { CalculateItemPrice(); });

        //Get tax percent
        taxPercent = GameManager.instance.GetTaxPercent();
        taxPercentText.text = taxPercent + " %";
        CalculateItemPrice();
    }

    public void ValueSliderChangeCheck(Slider slider)
    {

        //Debug.Log(" slider " + slider.name + " Slider percent value " + (slider.value * 100) / (attackSpeedSlider.value + criticalAttackSlider.value + attackSlider.value));

        attackResult = Mathf.CeilToInt((attackSlider.value * 100) / (attackSpeedSlider.value + criticalAttackSlider.value + attackSlider.value));
        attackSpeedResult = Mathf.CeilToInt((attackSpeedSlider.value * 100) / (attackSpeedSlider.value + criticalAttackSlider.value + attackSlider.value));
        critResult = Mathf.CeilToInt((criticalAttackSlider.value * 100) / (attackSpeedSlider.value + criticalAttackSlider.value + attackSlider.value));

        //SetSlidersTextResultValue();
        CalculateItemPrice();

    }


    private void SetSlidersTextResultValue()
    {
        attackPercent.text = attackResult.ToString();
        attackSpeedPercent.text = attackSpeedResult.ToString();
        criticalAttackPercent.text = critResult.ToString();
        //Debug.Log("Attack " + attackResult + " attackSpeed " + attackSpeedResult + " critical result " + critResult);
    }


    public void HandleCloseItemCreationWindow()
    {
        Destroy(gameObject);
    }


    public void HandleCreateItem()
    {
        Debug.Log("Item created");
        SendItemInfoForCreating();
        Destroy(gameObject);
    }

    private void CheckTogglesActivity()
    {
        //Can i remove it to switch?
        if (fireToggle.isOn == true )
        {
            choisenElement = Item.Elements.Fire;
            currentPrice += 10;
        }
        else if (iceToggle.isOn == true )
        {
            choisenElement = Item.Elements.Ice;
            currentPrice += 20;
        }
        else if(electricityToggle.isOn == true)
        {
            choisenElement = Item.Elements.Electricity;
            currentPrice += 30;
        }
        else
        {
            choisenElement = Item.Elements.WithoutElement;
        }
    }

    private void CheckProfitToggles()
    {
        if (fivePercentProfitToggle.isOn == true)
        {
            profitSumm = (currentPrice * 5) / 100;
        }
        else if (tenPercentProfitToggle.isOn == true)
        {
            profitSumm = (currentPrice * 10) / 100;
        }
        else if (fifteenPercentProfitToggle.isOn == true)
        {
            profitSumm = (currentPrice * 15) / 100;
        }
        else
        {
            profitSumm = (currentPrice * 30) / 100;
        }
        currentPrice += profitSumm;
    }

    private void CalculateTaxSumm()
    {
        taxSumm = (profitSumm * taxPercent) / 100;
        if (taxSumm <= 0)
        {
            taxSumm = 1;
        }
        currentPrice += taxSumm;
    }

    private void CalculateItemPrice()
    {
        currentPrice = attackMaterialPrice * attackResult + attackSpeedMaterialPrice * attackSpeedResult + attackCriticalMaterialPrice * critResult;
        CheckTogglesActivity();
        CheckProfitToggles();
        CalculateTaxSumm();
        UpdatePanelUI();
    }

    private void UpdatePanelUI()
    {
        profitSummText.text = profitSumm.ToString();
        taxSummText.text = taxSumm.ToString();
        totalSummText.text = currentPrice.ToString();
        SetSlidersTextResultValue();
    }

    public void SendItemInfoForCreating()
    {
        UIController.instance.OnItemCreate(attackResult,attackSpeedResult,critResult,weaponName.text,currentPrice, choisenElement, currentType);
    }
}
