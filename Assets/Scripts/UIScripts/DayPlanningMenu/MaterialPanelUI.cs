﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MaterialPanelUI : MonoBehaviour {

    public enum MaterialType
    {
        Metall, Wood, Crystall, Bowstring
    }

    public MaterialType materialType;
    public Button increaseButton;
    public Button decreaseButton;
    public TMP_InputField materialQuantityText;
    private int quantity = 0;
    private int moneyAccountValue;
    private int materialPrice;

    private void Start()
    {
        //нельяз так - слишком грубая связь. От младшего сразу к самому старшему.
        moneyAccountValue = GameManager.instance.GetMoneyValue();
    }

    //TODO add checkout for value. If we have not enough money - block increasing value here and in input field
    //Просто вычитать из счета сумму материала. Вопрос в одном - где взять этот счет? в GameManager или в поджительсок панели - что является более верным вариантом.
    public void IncreaseValue()
    {
        //materialQuantity.text = (int.Parse(materialQuantity.text) - 1).ToString();
        quantity++;
        UpdateUI();
    }

    public void DecreaseValue()
    {
        if (quantity>0)
        {
            quantity--;
        }
        UpdateUI();
    }

    public void CheckoutQuantityValue()
    {
        string value = materialQuantityText.text;
        if (!int.TryParse(value,out quantity))
        {
            Debug.LogError("INcorrect input value");
            materialQuantityText.text = "";
        }
    }

    private void UpdateUI()
    {
        materialQuantityText.text = quantity.ToString();
    }

}
