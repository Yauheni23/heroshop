﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopInventoryUI : MonoBehaviour {

    private StoreInventory store;

    public Transform shopParentUI;
    ShopSlotScript[] shopSlots;

    void Start()
    {
        store = StoreInventory.instance;
        store.onStoreInventoryChange += UpdateStoreUI;
        shopSlots = shopParentUI.GetComponentsInChildren<ShopSlotScript>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void UpdateStoreUI()
    {
        for (int i = 0; i < shopSlots.Length; i++)
        {
            if (i < store.storeItemsList.Count)
            {
                shopSlots[i].AddItemToStore(store.storeItemsList[i]);
            }
            else
            {
                shopSlots[i].ClearStoreSlot();
            }
        }
        Debug.Log("Updating UI");
    }

}
