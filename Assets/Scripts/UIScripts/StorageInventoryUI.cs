﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageInventoryUI : MonoBehaviour {

    StorageInventory storageInventory;
    public Transform storageParentUI;


    StorageSlotScript[] storageSlots;

	// Use this for initialization
	void Start () {

        storageInventory = StorageInventory.instance;
        storageInventory.onStorageChangeEvent += UpdateStorageUI;
        storageSlots = storageParentUI.GetComponentsInChildren<StorageSlotScript>();
        UpdateStorageUI();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void UpdateStorageUI()
    {
        for (int i = 0; i < storageSlots.Length; i++)
        {
            if (i < storageInventory.storageListItem.Count)
            {
                storageSlots[i].AddItemToStorage(storageInventory.storageListItem[i]);
            }
            else
            {
                storageSlots[i].ClearStorageSlot();
            }
        }
        Debug.Log("Updating UI");
    }
    public void OnClickCloseButton()
    {
        this.gameObject.SetActive(false);
    }
    
}
