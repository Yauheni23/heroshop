﻿using Assets.Scripts.LogicScripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIController : MonoBehaviour {

    public static UIController instance;

    public RectTransform createWindow;
    public RectTransform bowWindow;
    public RectTransform staffWindow;
    public RectTransform createdItemStatsInfoWindow;
    public RectTransform storageWindow;
    public Canvas mainCanvas;

    public List<Sprite> swordSprite = new List<Sprite>();
    public Sprite [] bowSprite = new Sprite[3];
    public Sprite [] staffSprite = new Sprite[3];


    ////UI buttons for each main character
    public RectTransform swordmasterButtonCreateUI;
    public RectTransform swordmasterButtonDialogueUI;
    public RectTransform bowmasterButtonCreateUI;
    public RectTransform bowmasterButtonDialogueUI;
    public RectTransform staffMasterButtonCreateUI;
    public RectTransform staffMasterButtonDialogueUI;
    //UI for dialogue
    public RectTransform swordmasterDialogueWindow;
    public RectTransform bowmasterDialogueWindow;
    public RectTransform staffmasterDialogueWindow;

    public Slider swordCreationSlider;
    public Slider bowCreationSlider;
    public Slider staffCreationSlider;
    private Queue<SwordItem> createdSwordsForShowingQueue = new Queue<SwordItem>();
    private Queue<BowItem> createdBowsForShowingQueue = new Queue<BowItem>();
    private Queue<StaffItem> createdStaffsForShowingQueue = new Queue<StaffItem>();
    //private delegate void CreateItemDelegate(Item item);
    //private CreateItemDelegate createItemDelegate;
    //private System.Action<Item> itemCreateCallback;
    //INstantiate creation windows
    public void OnShowButton()
    {
        Instantiate(createWindow, mainCanvas.transform);

    }
    public void OnBowButtonMenu()
    {
        Instantiate(bowWindow, mainCanvas.transform);
    }
    public void OnStaffButtonMenu()
    {
        Instantiate(staffWindow, mainCanvas.transform);
    }

    public void OnStorageWindowShow()
    {
        storageWindow.gameObject.SetActive(true);
    }




    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of UI controller");
            return;
        }

        instance = this;
    }


    private void Start()
    {
    }

    public Item GetCreatedSwordStats()
    {
        return createdSwordsForShowingQueue.Dequeue();
    }

    public Item GetCreatedBowStats()
    {
        return createdBowsForShowingQueue.Dequeue();
    }
    public Item GetCreatedStaffStats()
    {
        return createdStaffsForShowingQueue.Dequeue();
    }

    public void OnItemCreate(int attack, int attackSpeed, int crit, string name,int price, Item.Elements elemType, Item.ItemTypes itemType)
    {
        switch (itemType)
        {
            case Item.ItemTypes.Sword:
                createdSwordsForShowingQueue.Enqueue(StartCreationProcess(swordCreationSlider, new SwordItem(attack, attackSpeed, crit, elemType, name, price,itemType)) as SwordItem);
                //StartCreationProcess(swordCreationSlider, new SwordItem(attack, attackSpeed, crit, elemType, name, price, itemType));
                break;
            case Item.ItemTypes.Bow:
                createdBowsForShowingQueue.Enqueue(StartCreationProcess(bowCreationSlider, new BowItem(attack, attackSpeed, crit, elemType, name, price, itemType)) as BowItem);
                //StartCreationProcess(bowCreationSlider, new BowItem(attack, attackSpeed, crit, elemType, name, price, itemType));
                break;
            case Item.ItemTypes.Staff:
                createdStaffsForShowingQueue.Enqueue(StartCreationProcess(staffCreationSlider, new StaffItem(attack, attackSpeed, crit, elemType, name, price, itemType)) as StaffItem);
                //StartCreationProcess(staffCreationSlider, new StaffItem(attack, attackSpeed, crit, elemType, name, price, itemType));
                break;
            default:
                break;
        }
    }

    private void ShowCreatingItemForPlayer(Item createdItem)
    {
        // SwordItem swordItem = createdItem as SwordItem;
        //if (swordItem !=null)
        //{

        //    Debug.Log("Sword created " + swordItem.ItemName + swordItem.Attack);
        //}
        switch (createdItem.ItemType)
        {
            case Item.ItemTypes.Sword:
                SwordItem swordItem = createdItem as SwordItem;
                if (swordItem !=null)
                {
                    createdItem.ItemSprite = GetSwordIcon();
                    Instantiate(createdItemStatsInfoWindow, mainCanvas.transform);
                    Debug.Log("Sword created " + swordItem.ItemName + swordItem.Attack);
                }
                break;
            case Item.ItemTypes.Bow:
                BowItem bowItem = createdItem as BowItem;
                if (bowItem != null)
                {
                    createdItem.ItemSprite = GetBowIcon();
                    Instantiate(createdItemStatsInfoWindow, mainCanvas.transform);
                    Debug.Log("Bow created " + bowItem.ItemName + bowItem.Attack);
                }
                break;
            case Item.ItemTypes.Staff:
                StaffItem staffItem = createdItem as StaffItem;
                if (staffItem != null)
                {
                    createdItem.ItemSprite = GetStaffIcon();
                    Instantiate(createdItemStatsInfoWindow, mainCanvas.transform);
                    Debug.Log("Staff created " + staffItem.ItemName + staffItem.Attack);
                }
                break;
            default:
                break;
        }
    }

    private Sprite GetSwordIcon()
    {
        return swordSprite[Random.Range(0, swordSprite.Count-1)];
    }

    private Sprite GetBowIcon()
    {
        return bowSprite[Random.Range(0, bowSprite.Length - 1)];
    }

    private Sprite GetStaffIcon()
    {
        return staffSprite[Random.Range(0,staffSprite.Length-1)];
    }



    private Item StartCreationProcess(Slider slider, Item itemForCreate)
    {

        StartCoroutine(SliderLoader(slider, itemForCreate, item => ShowCreatingItemForPlayer(item)));
        return itemForCreate;
    }

    IEnumerator SliderLoader(Slider slider,Item item, System.Action<Item> callback)
    {
        slider.gameObject.SetActive(true);
        while (slider.value < slider.maxValue)
        {
            slider.value += 0.5f;
            yield return null;
        }
        slider.value = slider.minValue;
        slider.gameObject.SetActive(false);
        callback.Invoke(item);
    }

    public void ShowCharactersButtons(string characterType)
    {
        
        ;
        switch (characterType)
        {
            //case MainCharactersEnum.Swordmaster:
            case "Swordmaster":
                if (!swordmasterButtonCreateUI.gameObject.activeSelf)
                {
                    swordmasterButtonCreateUI.gameObject.SetActive(true);
                    swordmasterButtonDialogueUI.gameObject.SetActive(true);
                }
                else
                {
                    swordmasterButtonCreateUI.gameObject.SetActive(false);
                    swordmasterButtonDialogueUI.gameObject.SetActive(false);
                }
                break;
            //case MainCharactersEnum.Bowmaster:
            case "Bowmaster":
                if (!bowmasterButtonCreateUI.gameObject.activeSelf)
                {
                    bowmasterButtonCreateUI.gameObject.SetActive(true);
                    bowmasterButtonDialogueUI.gameObject.SetActive(true);
                }
                else
                {
                    bowmasterButtonCreateUI.gameObject.SetActive(false);
                    bowmasterButtonDialogueUI.gameObject.SetActive(false);
                }
                break;
            //case MainCharactersEnum.Staffmaster:
            case "Staffmaster":
                if (!staffMasterButtonCreateUI.gameObject.activeSelf)
                {
                    staffMasterButtonCreateUI.gameObject.SetActive(true);
                    staffMasterButtonDialogueUI.gameObject.SetActive(true);
                }
                else
                {
                    staffMasterButtonCreateUI.gameObject.SetActive(false);
                    staffMasterButtonDialogueUI.gameObject.SetActive(false);
                }
                break;
            default:
                break;
        }


    }
}
