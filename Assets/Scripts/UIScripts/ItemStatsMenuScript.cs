﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class ItemStatsMenuScript : MonoBehaviour {

    public TMP_Text weaponName;
    public Button saveWeaponButton;
    public Button destroyWindowButton;
    public TMP_Text attackResult;
    public TMP_Text attackSpeedResult;
    public TMP_Text criticalAttackResult;
    public Image backgroundItemImage;
    public Image itemImage;
    public Sprite fireBackgroundSprite;
    public Sprite iceBackgroundSprite;
    public Sprite electricityBackgroundSprite;
    private Item item;

    public void Start()
    {
        saveWeaponButton.onClick.AddListener(HandleSaveItem);
        destroyWindowButton.onClick.AddListener(HandleDeleteItemStatsWindow);
        //TODO add method to receive data from UIController and show its in window
        item = UIController.instance.GetCreatedSwordStats();
        SetBackgroundImage();
        UpdateWindowUI();

    }

    private void UpdateWindowUI()
    {
        weaponName.text = item.ItemName.ToString();
        attackResult.text = item.Attack.ToString();
        attackSpeedResult.text = item.AttackSpeed.ToString();
        criticalAttackResult.text = item.CriticalDamage.ToString();
        itemImage.sprite = item.ItemSprite;


    }

    private void SetBackgroundImage()
    {
        switch (item.Element)
        {
            case Item.Elements.Fire:
                backgroundItemImage.sprite = fireBackgroundSprite;
                break;
            case Item.Elements.Ice:
                backgroundItemImage.sprite = iceBackgroundSprite;
                break;
            case Item.Elements.Electricity:
                backgroundItemImage.sprite = electricityBackgroundSprite;
                break;
            case Item.Elements.WithoutElement:
                backgroundItemImage.sprite = fireBackgroundSprite;
                break;
            default:
                break;
        }

    }

    private void HandleSaveItem()
    {
        StorageInventory.instance.AddToStorage(item);
        Destroy(gameObject);
        //Debug.Log("Save item to inventory" + item.Attack + item.ItemName);
    }

    private void HandleDeleteItemStatsWindow()
    {
        Destroy(gameObject);
    }


}
