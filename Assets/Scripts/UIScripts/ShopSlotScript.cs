﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopSlotScript : MonoBehaviour {

    public Image shopItemImage;
    public Text attackText;

    Item item;

    public void AddItemToStore(Item newItem)
    {
        item = newItem;

        attackText.text = item.Attack.ToString();

        shopItemImage.gameObject.SetActive(true);
    }

    public void ClearStoreSlot()
    {
        item = null;
        //storageItemImage.sprite = null;
        //storageItemImage.enabled = false;
        shopItemImage.gameObject.SetActive(false);
        attackText.text = null;

    }

}
