﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StorageSlotScript : MonoBehaviour {

    public Image storageItemImage;
    public Image storageItemBackground;
    //public Text attackText;

    Item item;

    public void AddItemToStorage(Item newItem)
    {
        item = newItem;

        //attackText.text = item.Attack.ToString();

        //storageItemImage.gameObject.SetActive(true);
        storageItemImage.sprite = item.ItemSprite;
    }
    //private void SetBackgroundImage()
    //{
    //    switch (item.Element)
    //    {
    //        case Item.Elements.Ice:
    //            storageItemBackground.sprite = 
    //            break;
    //        case Item.Elements.Fire:
    //            storageItemBackground.sprite =
    //            break;
    //        case Item.Elements.Electricity:
    //            storageItemBackground.sprite =
    //            break;
    //        case Item.Elements.WithoutElement:
    //            storageItemBackground.sprite =
    //            break;
    //        default:
    //            break;
    //    }

    //}

    public void ClearStorageSlot()
    {
        item = null;
        //storageItemImage.sprite = null;
        //storageItemImage.enabled = false;
        storageItemImage.sprite = null;
        //attackText.text = null;

    }

}
