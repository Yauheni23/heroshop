﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item {

    public enum Elements
    {
        Ice,
        Fire,
        Electricity,
        WithoutElement
    }

    public enum ItemTypes
    {
        Sword,
        Bow,
        Staff
    }

    public virtual Sprite ItemSprite { get; set; }
    public virtual Sprite ItemBackground { get; set; }
    public virtual int Price { get;  set; }
    public virtual string ItemName { get; set; }
    public virtual int Attack { get; protected  set; }
    public virtual int AttackSpeed { get; set; }
    public virtual int CriticalDamage { get; set; }
    public virtual Elements Element { get; set; }
    public virtual ItemTypes ItemType { get; set; }

    public virtual void GetBaseMessage()
    {
        Debug.Log("Item class");
    }



}
