﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageInventory : MonoBehaviour {

    public static StorageInventory instance;

    public List<Item> storageListItem = new List<Item>();

    public delegate void OnStorageChange();
    public OnStorageChange onStorageChangeEvent;


    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of storage inventory");
            return;
        }

        instance = this;
    }

    public void AddToStorage(Item item)
    {

        storageListItem.Add(item);

        if (onStorageChangeEvent != null)
        {
            onStorageChangeEvent.Invoke();
        }
    }


    public void RemoveItemFromShop(Item item)
    {
        storageListItem.Remove(item);

        if (onStorageChangeEvent != null)
        {
            onStorageChangeEvent();
        }
    }
}
