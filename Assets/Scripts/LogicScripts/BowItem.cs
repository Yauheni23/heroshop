﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.LogicScripts
{
    class BowItem : Item
    {
        public int StaminaUsage { get; set; }


        public override int Attack
        {
            get
            {
                return base.Attack;
            }

            protected set
            {
                base.Attack = value;
            }
        }

        public override int AttackSpeed
        {
            get
            {
                return base.AttackSpeed;
            }

            set
            {
                base.AttackSpeed = value;
            }
        }

        public override int CriticalDamage
        {
            get
            {
                return base.CriticalDamage;
            }

            set
            {
                base.CriticalDamage = value;
            }
        }
        public override string ItemName
        {
            get
            {
                return base.ItemName;
            }

            set
            {
                base.ItemName = value;
            }
        }

        public override Elements Element
        {
            get
            {
                return base.Element;
            }

            set
            {
                base.Element = value;
            }
        }

        public override int Price
        {
            get
            {
                return base.Price;
            }

            set
            {
                base.Price = value;
            }
        }

        public BowItem(int attack, int attackSpeed, int critDamage, Elements elem, string name, int price, ItemTypes itemType)
        {
            Attack = attack;
            AttackSpeed = attackSpeed;
            CriticalDamage = critDamage;
            Element = elem;
            ItemName = name;
            Price = price;
            base.ItemType = itemType;
        }

    }
}
