﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreInventory : MonoBehaviour {

    public static StoreInventory instance;

    public List<Item> storeItemsList = new List<Item>();

    public delegate void OnStoreItemChange();
    public OnStoreItemChange onStoreInventoryChange;

    void Awake()
    {
        if (instance!=null)
        {
            Debug.LogWarning("More than one instance store inventory");
            return;
        }

        instance = this;
    }


    public void AddItemToShop(Item item)
    {
        storeItemsList.Add(item);
    }

    public void RemoveItemFromShop(Item item)
    {
        storeItemsList.Remove(item);
    }
}
