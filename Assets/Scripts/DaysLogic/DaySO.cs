﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New game day", menuName = "Day/New day")]
public class DaySO : ScriptableObject
{
    public NewsScript newsOfTheDay;
    public List<UniqueCustomers> uniqueCustomersList = new List<UniqueCustomers>();
    public List<Characters> charactersOfTheDay = new List<Characters>();

}
