﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{

    //private int metallQuantity;
    //private int woodQuantity;
    //private int cristallsQuantity;
    //private int bowstringQuantity;
    //private int extraMetallQuantity;
    public RectTransform daysWindow;
    public TMP_Text daysText;
    public static GameManager instance;
    private DaySO currentDay;
    private int currentDayNumber;
    public List<DaySO> daysList = new List<DaySO>();

    //Dialogue lists TODO maybe add new way to adding dialogues in each day. Or reorgonize position of dialogues list in day object to acess by index
    //It's best create editor menu for creating days in one window
    private List<string> swordmasterDialogueList = new List<string>();
    private List<string> bowmasterDialogueList = new List<string>();
    private List<string> staffmasterDialogueList = new List<string>();

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance store inventory");
            return;
        }

        instance = this;
    }

    void Start()
    {
        if (currentDay == null)
        {
            currentDay = daysList[0];
        }
    }

    void Update()
    {


    }

    public void LoadNewDay()
    {
        StartCoroutine(DayMenuShowTime());
    }

    IEnumerator DayMenuShowTime()
    {

        if (currentDayNumber == daysList.Count)
        {
            //TODO disable Button. Don't Allow user go an another day
            //End of the game
            Debug.Log("GAME END");
        }
        else
        {
            daysWindow.gameObject.SetActive(true);
            daysText.text = "DAY " + (currentDayNumber + 1).ToString();
            yield return new WaitForSeconds(5);
            daysWindow.gameObject.SetActive(false);
            currentDayNumber += 1;
            currentDay = daysList[currentDayNumber - 1];
        }



    }


    //TODO add tax given percent
    public int GetTaxPercent()
    {
        
        return 15;
    }

    public int GetMoneyValue()
    {
        return 5000;
    }

}
